
package com.example.types;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyDescription;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;


/**
 * vendor data info
 * 
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "infoCode",
    "infoValue"
})
public class VendorDataInfoList {

    /**
     * coded value to identify additional info specific to the firm
     * (Required)
     * 
     */
    @JsonProperty("infoCode")
    @JsonPropertyDescription("coded value to identify additional info specific to the firm")
    private String infoCode;
    /**
     * the value of additional information identified by infocode
     * (Required)
     * 
     */
    @JsonProperty("infoValue")
    @JsonPropertyDescription("the value of additional information identified by infocode")
    private String infoValue;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * coded value to identify additional info specific to the firm
     * (Required)
     * 
     */
    @JsonProperty("infoCode")
    public String getInfoCode() {
        return infoCode;
    }

    /**
     * coded value to identify additional info specific to the firm
     * (Required)
     * 
     */
    @JsonProperty("infoCode")
    public void setInfoCode(String infoCode) {
        this.infoCode = infoCode;
    }

    /**
     * the value of additional information identified by infocode
     * (Required)
     * 
     */
    @JsonProperty("infoValue")
    public String getInfoValue() {
        return infoValue;
    }

    /**
     * the value of additional information identified by infocode
     * (Required)
     * 
     */
    @JsonProperty("infoValue")
    public void setInfoValue(String infoValue) {
        this.infoValue = infoValue;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("infoCode", infoCode).append("infoValue", infoValue).append("additionalProperties", additionalProperties).toString();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(infoCode).append(infoValue).append(additionalProperties).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof VendorDataInfoList) == false) {
            return false;
        }
        VendorDataInfoList rhs = ((VendorDataInfoList) other);
        return new EqualsBuilder().append(infoCode, rhs.infoCode).append(infoValue, rhs.infoValue).append(additionalProperties, rhs.additionalProperties).isEquals();
    }

}
