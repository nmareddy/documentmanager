
package com.example.types;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyDescription;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;


/**
 * activity
 * 
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "activityTimestamp",
    "activityCode",
    "vendorDataInfoList"
})
public class ActivityInput {

    /**
     * timestamp when the activity took place
     * 
     */
    @JsonProperty("activityTimestamp")
    @JsonPropertyDescription("timestamp when the activity took place")
    private String activityTimestamp;
    /**
     * activity code
     * (Required)
     * 
     */
    @JsonProperty("activityCode")
    @JsonPropertyDescription("activity code")
    private String activityCode;
    /**
     * Listof vendor data
     * (Required)
     * 
     */
    @JsonProperty("vendorDataInfoList")
    @JsonPropertyDescription("Listof vendor data")
    private List<VendorDataInfoList> vendorDataInfoList = new ArrayList<VendorDataInfoList>();
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * timestamp when the activity took place
     * 
     */
    @JsonProperty("activityTimestamp")
    public String getActivityTimestamp() {
        return activityTimestamp;
    }

    /**
     * timestamp when the activity took place
     * 
     */
    @JsonProperty("activityTimestamp")
    public void setActivityTimestamp(String activityTimestamp) {
        this.activityTimestamp = activityTimestamp;
    }

    /**
     * activity code
     * (Required)
     * 
     */
    @JsonProperty("activityCode")
    public String getActivityCode() {
        return activityCode;
    }

    /**
     * activity code
     * (Required)
     * 
     */
    @JsonProperty("activityCode")
    public void setActivityCode(String activityCode) {
        this.activityCode = activityCode;
    }

    /**
     * Listof vendor data
     * (Required)
     * 
     */
    @JsonProperty("vendorDataInfoList")
    public List<VendorDataInfoList> getVendorDataInfoList() {
        return vendorDataInfoList;
    }

    /**
     * Listof vendor data
     * (Required)
     * 
     */
    @JsonProperty("vendorDataInfoList")
    public void setVendorDataInfoList(List<VendorDataInfoList> vendorDataInfoList) {
        this.vendorDataInfoList = vendorDataInfoList;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("activityTimestamp", activityTimestamp).append("activityCode", activityCode).append("vendorDataInfoList", vendorDataInfoList).append("additionalProperties", additionalProperties).toString();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(activityTimestamp).append(activityCode).append(vendorDataInfoList).append(additionalProperties).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof ActivityInput) == false) {
            return false;
        }
        ActivityInput rhs = ((ActivityInput) other);
        return new EqualsBuilder().append(activityTimestamp, rhs.activityTimestamp).append(activityCode, rhs.activityCode).append(vendorDataInfoList, rhs.vendorDataInfoList).append(additionalProperties, rhs.additionalProperties).isEquals();
    }

}
