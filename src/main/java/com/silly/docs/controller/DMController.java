package com.silly.docs.controller;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;

import org.apache.commons.io.FilenameUtils;
import org.bson.BsonBinarySubType;
import org.bson.types.Binary;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.client.MongoClient;
import com.mongodb.gridfs.GridFS;
import com.mongodb.gridfs.GridFSDBFile;
import com.silly.docs.dto.UserDoc;
import com.silly.docs.dto.UserDocumentInfo;
import com.silly.docs.model.UserDocumentInfoResource;
import com.silly.docs.repository.UserDocInfoRepository;
import com.silly.docs.repository.UserDocRepository;

@RestController
@RequestMapping("/docm/v1")
public class DMController {

    private UserDocInfoRepository userDocInfoRepository;
    private UserDocRepository userDocRepository;
    

    public DMController(UserDocInfoRepository userDocInfoRepository,UserDocRepository userDocRepository) {
        this.userDocInfoRepository = userDocInfoRepository;
        this.userDocRepository = userDocRepository;
    }

    @PostMapping("/upload")
    public String singleFileUpload(@RequestParam("file") MultipartFile multipart, @RequestParam("email") String email,@RequestParam("docType") String docType,@RequestParam("empId") String empId) {
        try {
           UserDocumentInfo demoDocument = new UserDocumentInfo();
            demoDocument.setEmail(email);
            demoDocument.setDocType(docType);
            demoDocument.setEmpId(new Long(empId));
           String uID =  UUID.randomUUID().toString();
           String extension = FilenameUtils.getExtension(multipart.getOriginalFilename());
           demoDocument.setFileExtension(extension);
           String fileName = StringUtils.cleanPath(multipart.getOriginalFilename());
           demoDocument.setFileName(fileName);
           long fileSize = multipart.getSize();
           demoDocument.setFileSize(fileSize);
            demoDocument.setuID(uID);
            UserDoc doc = new UserDoc();
            doc.setFile(new Binary(BsonBinarySubType.BINARY, multipart.getBytes()));
            doc.setuID(uID);
            doc.setFileExtension(extension);
            doc.setFileName(fileName);
            doc.setFileSize(fileSize);
           // demoDocument.setFile(new Binary(BsonBinarySubType.BINARY, multipart.getBytes()));
            userDocRepository.insert(doc);
            userDocInfoRepository.insert(demoDocument);
            System.out.println(demoDocument);
        } catch (Exception e) {
            e.printStackTrace();
            return "failure";
        }
        return "success";
    }
    
    @PostMapping("/uploadMultiple")
    public List<String> uploadMultipleFiles(@RequestParam("files") MultipartFile[] files, @RequestParam("email") String email,@RequestParam("docType") String docType,@RequestParam("empId") String empId) {
        return Arrays.asList(files)
                .stream()
                .map(file -> singleFileUpload(file, email,docType,empId))
                .collect(Collectors.toList());
    }
    
    @GetMapping("/retrieve/{id}")
    public ResponseEntity<UserDocumentInfo> retrieveFile(@PathVariable String id) {
    	Optional<UserDocumentInfo> doc = userDocInfoRepository.findByEmpId(new Long(id));
    	if(doc!=null && doc.get()!=null) {
    		return ResponseEntity.ok(doc.get());
    	}
return ResponseEntity.badRequest().body(null);
    
    }
    
    /*@RequestMapping(value = "/download/{uId}", 
    	    method = RequestMethod.GET, 
    	    produces = { MediaType.APPLICATION_OCTET_STREAM_VALUE })
    	@ResponseBody FileSystemResource downloadFile(String uId) {
    	return null;
    }*/
    @GetMapping("/download/{uId}")
    @Produces(MediaType.APPLICATION_OCTET_STREAM_VALUE)
    public Response downloadFilebyID(@PathVariable  String uId) throws IOException {
      Response response = null;
      
     UserDoc doc =  userDocRepository.findByUID(uId);
     doc.getFile();     
      if (doc.getFile() != null ) {
    	  InputStream in = new ByteArrayInputStream(doc.getFile().getData());
          ByteArrayOutputStream out = new ByteArrayOutputStream();
            int data = in.read();
            while (data >= 0) {
              out.write((char) data);
              data = in.read();
            }
          out.flush();
          
          FileOutputStream fos = null;
          try {
              fos = new FileOutputStream(new File(doc.getFileName())); 
              //ByteArrayOutputStream baos = new ByteArrayOutputStream();

              // Put data in your baos

              out.writeTo(fos);
          } catch(IOException ioe) {
              // Handle exception here
              ioe.printStackTrace();
          } finally {
              fos.close();
          }
          ResponseBuilder builder = Response.ok(out.toByteArray());
          builder.header("Content-Disposition", "attachment; filename=" + doc.getFileName());
          response = builder.build();
          } else {
            response = Response.status(404).entity(" Unable to get file with ID: " + uId).type("text/plain").build();
          }
        return response;
    }
    
    @GetMapping("/retrieveAll")
    public ResponseEntity< List<UserDocumentInfoResource> > retrieveFiles(){
    	final List < UserDocumentInfo > collection = userDocInfoRepository.findAll();
    	UserDocumentInfoResource res = null;
    final List < UserDocumentInfoResource > resources = new ArrayList<UserDocumentInfoResource>();
 for(UserDocumentInfo info :collection) {
	 if(info!=null) {
		 res =  new UserDocumentInfoResource(info);
		 resources.add(res);
	 }
 }
   // final String uriString = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();

   // resources.add(new Link(uriString, "self"));

    return ResponseEntity.ok(resources);}
}