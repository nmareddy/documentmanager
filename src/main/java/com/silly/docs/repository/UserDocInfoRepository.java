package com.silly.docs.repository;


import java.util.Optional;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.silly.docs.dto.UserDoc;
import com.silly.docs.dto.UserDocumentInfo;

public interface UserDocInfoRepository extends MongoRepository<UserDocumentInfo, Integer> {
	
	public Optional<UserDocumentInfo>  findByEmail(String email);
	public Optional<UserDocumentInfo>  findByDocType(String docType);
	public Optional<UserDocumentInfo>  findByEmpId(Long empID);
}