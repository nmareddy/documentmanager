package com.silly.docs.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.silly.docs.dto.UserDoc;

public interface UserDocRepository extends MongoRepository<UserDoc, Integer> {

	
	public UserDoc findByUID(String uID);
}
