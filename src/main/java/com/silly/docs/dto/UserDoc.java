package com.silly.docs.dto;

import org.bson.types.Binary;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

@Document
public class UserDoc {

	@Id
    @Field
    private String id;
    @Field
    private Binary file;
    
    @Field
    private String uID;
    
    @Field
    private String fileName;
    
    @Field
    private long fileSize;
    
    @Field
    private String fileExtension;
    
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

	public Binary getFile() {
		return file;
	}

	public void setFile(Binary file) {
		this.file = file;
	}

	public String getuID() {
		return uID;
	}

	public void setuID(String uID) {
		this.uID = uID;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public long getFileSize() {
		return fileSize;
	}

	public void setFileSize(long fileSize) {
		this.fileSize = fileSize;
	}

	public String getFileExtension() {
		return fileExtension;
	}

	public void setFileExtension(String fileExtension) {
		this.fileExtension = fileExtension;
	}

    
}