package com.silly.docs.model;

import com.silly.docs.dto.UserDocumentInfo;

public class UserDocumentInfoResource {

	
	 private String email;

	   
	    private String docType;    
	    
	    
	    private Long empId;
	    
	    
	    private String uID;
	    
	    private String link;
	    
	    private String fileName;
	    
	    private String fileSize;
	    
	    private String fileExtension;
	    
	    
	    public UserDocumentInfoResource(UserDocumentInfo info) {
	    	this.email = info.getEmail();
	    	this.docType = info.getDocType();
	    	this.empId = info.getEmpId();
	    	this.link = "/docm/v1/download/"+info.getuID();
	    	this.uID = info.getuID();
	    	this.fileExtension = info.getFileExtension();
	    	this.fileName = info.getFileName();
	    	if(new Long(info.getFileSize())!=null)
	    	this.fileSize = new Long(info.getFileSize()).toString();
	    }


		public String getEmail() {
			return email;
		}


		public void setEmail(String email) {
			this.email = email;
		}


		public String getDocType() {
			return docType;
		}


		public void setDocType(String docType) {
			this.docType = docType;
		}


		public Long getEmpId() {
			return empId;
		}


		public void setEmpId(Long empId) {
			this.empId = empId;
		}


		public String getuID() {
			return uID;
		}


		public void setuID(String uID) {
			this.uID = uID;
		}


		public String getLink() {
			return link;
		}


		public void setLink(String link) {
			this.link = link;
		}


		public String getFileName() {
			return fileName;
		}


		public void setFileName(String fileName) {
			this.fileName = fileName;
		}


		public String getFileSize() {
			return fileSize;
		}


		public void setFileSize(String fileSize) {
			this.fileSize = fileSize;
		}


		public String getFileExtension() {
			return fileExtension;
		}


		public void setFileExtension(String fileExtension) {
			this.fileExtension = fileExtension;
		}
	    
	    
}
