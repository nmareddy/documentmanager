package com.silly.docs.config;


import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

import com.silly.docs.dto.UserDoc;
import com.silly.docs.repository.UserDocInfoRepository;

@EnableMongoRepositories(basePackageClasses = UserDocInfoRepository.class)
@Configuration
public class MongoDbConfig {


    @Bean
    CommandLineRunner commandLineRunner(UserDocInfoRepository userRepository) {
        return strings -> {
            //userRepository.save(new UserDoc(1, "Peter", "Development", 3000L));
            //userRepository.save(new UserDoc(2, "Sam", "Operations", 2000L));
        };
    }

}
